from konlpy.tag import Twitter
from collections import Counter
import pytagcloud
import re
import os

### 카페에서 할 짓도 없고 로직생각하기도 귀찮아서 대충 짬...###
### 추후 수정 예정                                        ###

def text_trim():
    trims = ""
    f = open("KakaoTalkChats.txt", "r")
    text = f.read()
    strtext = str(text)
    f.close()
    strtext = strtext.split("\n")
    for i in range(0, len(strtext), 1):
        if((strtext[i].find("오전") or strtext[i].find("오후")) and (strtext.count(":") != 1 or strtext.count(":") != 0)):
            if((strtext[i].find("샵검색") != -1) or (strtext[i].find("톡게시판") != -1) or strtext[i].endswith("사진") or strtext[i].endswith("이모티콘")):
                continue
            frontindex = strtext[i].find(":")
            tmptext = strtext[i][frontindex+1:]
            frontindex = tmptext.find(":")
            tmptext = tmptext[frontindex+2:]
            trims += tmptext
    f = open("kakaotrims.txt", "w")
    f.write(trims)
    f.close()

def imagemake():
    f = open("kakaotrims.txt", "r")
    text = f.read()
    nlp = Twitter()
    nouns = nlp.nouns(text)
    words = Counter(nouns)
    tags = words.most_common(80)
    taglist = pytagcloud.make_tags(tags, maxsize=100)
    pytagcloud.create_tag_image(taglist, "wordcloud.jpg", size=(640, 480), fontname="Korean", rectangular=True)

#file_checker()
text_trim()
imagemake()